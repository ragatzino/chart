{{/*
Expand the name of the chart.
*/}}
{{- define "dependabot-gitlab.name" -}}
{{ include "common.names.name" . }}
{{- end }}

{{/*
Create a default fully qualified app name.
*/}}
{{- define "dependabot-gitlab.fullname" -}}
{{ include "common.names.fullname" . }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "dependabot-gitlab.chart" -}}
{{ include "common.names.chart" . }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "dependabot-gitlab.labels" -}}
{{ include "common.labels.standard" . }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "dependabot-gitlab.selectorLabels" -}}
{{ include "common.labels.matchLabels" . }}
{{- end }}

{{/*
Redis name
*/}}
{{- define "dependabot-gitlab.redisName" -}}
{{- $redis := dict "Values" .Values.redis "Chart" (dict "Name" "redis") "Release" .Release -}}
{{ include "common.names.fullname" $redis }}
{{- end }}

{{/*
Mongodb name
*/}}
{{- define "dependabot-gitlab.mongodbName" -}}
{{- $mongodb := dict "Values" .Values.mongodb "Chart" (dict "Name" "mongodb") "Release" .Release -}}
{{ include "common.names.fullname" $mongodb }}
{{- end }}

{{/*
Redis svc name
*/}}
{{- define "dependabot-gitlab.redisSvcName" -}}
{{- if and (eq .Values.redis.architecture "replication") (not .Values.redis.sentinel.enabled) -}}
{{ printf "%s-replicas" (include "dependabot-gitlab.redisName" .) }}
{{- else if not .Values.redis.sentinel.enabled -}}
{{ printf "%s-master" (include "dependabot-gitlab.redisName" .) }}
{{- else -}}
{{ include "dependabot-gitlab.redisName" . }}
{{- end -}}
{{- end -}}

{{/*
Base config path
*/}}
{{- define "dependabot-gitlab.base-config-path" -}}
{{ print "/home/dependabot/app/config/dependabot" }}
{{- end -}}

{{/*
Pod annotations
*/}}
{{- define "dependabot-gitlab.podAnnotations" -}}
checksum/config: {{ include (print $.Template.BasePath "/configmap.yaml") . | sha256sum }}
{{- if not .Values.credentials.existingSecret }}
checksum/secrets: {{ include (print $.Template.BasePath "/secrets.yaml") . | sha256sum }}
{{- end }}
{{- if and .Values.redis.auth.enabled (not .Values.redis.auth.existingSecret) }}
checksum/redis-password: {{ default (randAlphaNum 10) .Values.redis.auth.password | sha256sum }}
{{- end }}
{{- if and .Values.mongodb.auth.enabled (not .Values.mongodb.auth.existingSecret) }}
checksum/mongodb-password: {{ default (randAlphaNum 10) (print .Values.mongodb.auth.rootPassword .Values.mongodb.auth.passwords) | sha256sum }}
{{- end }}
{{- if omit .Values.registriesCredentials "existingSecret" }}
checksum/registries-secrets: {{ include (print $.Template.BasePath "/registries-secrets.yaml") . | sha256sum }}
{{- end }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "dependabot-gitlab.serviceAccountName" -}}
{{- if .Values.serviceAccount.create -}}
  {{ default (include "dependabot-gitlab.fullname" .) .Values.serviceAccount.name }}
{{- else -}}
  {{ default "default" .Values.serviceAccount.name }}
{{- end -}}
{{- end -}}

{{/*
Environment config
This is slightly brittle as it relies on internal implementation of constructing redis and mongodb secret name
Not calling secret name helper functions directly due to issues with tpl function
*/}}
{{- define "dependabot-gitlab.database-credentials" -}}
{{- if and .Values.redis.enabled .Values.redis.auth.enabled -}}
- name: REDIS_PASSWORD
  valueFrom:
    {{- $redis := dict "Values" .Values.redis "Chart" (dict "Name" "redis") "Release" .Release }}
    secretKeyRef:
      name: {{ default (include "common.names.fullname" $redis) .Values.redis.auth.existingSecret }}
      key: {{ default "redis-password" .Values.redis.auth.existingSecretPasswordKey }}
{{- end }}
{{- if and .Values.mongodb.enabled .Values.mongodb.auth.enabled }}
- name: MONGODB_PASSWORD
  valueFrom:
    {{- $mongodb := dict "Values" .Values.mongodb "Chart" (dict "Name" "mongodb") "Release" .Release "Template" .Template }}
    secretKeyRef:
      name: {{ default (include "common.names.fullname" $mongodb) .Values.mongodb.auth.existingSecret }}
      key: mongodb-passwords
{{- end }}
{{- end }}

{{/*
Registries credentials configured
*/}}
{{- define "dependabot-gitlab.registries-credentials" -}}
{{- if or (omit .Values.registriesCredentials "existingSecret") (.Values.registriesCredentials.existingSecret) -}}
  {{- true -}}
{{- end -}}
{{- end -}}

{{/*
Projects
*/}}
{{- define "dependabot-gitlab.projects" -}}
{{- join " " .Values.projects }}
{{- end }}

{{/*
Image data
*/}}
{{- define "dependabot-gitlab.image" -}}
image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}"
imagePullPolicy: {{ .Values.image.pullPolicy }}
{{- end }}

{{/*
Migration job wait container
*/}}
{{- define "dependabot-gitlab.migrationsWaitContainer" -}}
- name: wait-migrations
  {{- include "dependabot-gitlab.image" . | nindent 2 }}
  args:
    - "rake"
    - "dependabot:check_migrations"
  {{- with (include "dependabot-gitlab.database-credentials" .) }}
  env:
    {{- . | nindent 4 }}
  {{- end }}
  envFrom:
    - configMapRef:
        name: {{ include "dependabot-gitlab.fullname" . }}
    - secretRef:
    {{- if .Values.credentials.existingSecret }}
        name: {{ .Values.credentials.existingSecret }}
    {{- else }}
        name: {{ include "dependabot-gitlab.fullname" . }}
    {{- end }}
  {{- with .Values.migrationsWaitContainer.resources }}
  resources:
    {{- toYaml . | nindent 12 }}
  {{- end }}
{{- end }}

{{/*
Redis wait container
*/}}
{{- define "dependabot-gitlab.redisWaitContainer" -}}
- name: wait-redis
  {{- include "dependabot-gitlab.image" . | nindent 2 }}
  args:
    - "rake"
    - "dependabot:check_redis"
  {{- with (include "dependabot-gitlab.database-credentials" .) }}
  env:
    {{- . | nindent 4 }}
  {{- end }}
  envFrom:
    - configMapRef:
        name: {{ include "dependabot-gitlab.fullname" . }}
    - secretRef:
    {{- if .Values.credentials.existingSecret }}
        name: {{ .Values.credentials.existingSecret }}
    {{- else }}
        name: {{ include "dependabot-gitlab.fullname" . }}
    {{- end }}
  {{- with .Values.redisWaitContainer.resources }}
  resources:
    {{- toYaml . | nindent 12 }}
  {{- end }}
{{- end }}

{{/*
Base configuration configured
*/}}
{{- define "dependabot-gitlab.base-config" -}}
{{- or (omit .Values.baseConfiguration "existingSecret") .Values.baseConfiguration.existingSecret -}}
{{- end }}

{{/*
Base configuration volume
*/}}
{{- define "dependabot-gitlab.base-config-volume" -}}
- name: base-config
  {{- if .Values.baseConfiguration.existingSecret }}
  secret:
    secretName: {{ .Values.baseConfiguration.existingSecret }}
  {{- else }}
  configMap:
    name: {{ include "dependabot-gitlab.fullname" . }}-base-config
{{- end }}
    items:
      - key: dependabot-base.yml
        path: dependabot-base.yml
{{- end }}

{{/*
Base configuration volume mount
*/}}
{{- define "dependabot-gitlab.base-config-mount" -}}
- name: base-config
  mountPath: {{ include "dependabot-gitlab.base-config-path" . }}
{{- end }}
